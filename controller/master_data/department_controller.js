const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    if (req.method !== 'GET') return res.status(405).end();
    const { data } = req.query;

    let result;
    if (data == 'active') {
        result = await knex('departments').where({ active: '1' }).orderBy('id', 'asc');
    }
    else {
        result = await knex('departments').orderBy('id', 'asc');
    }
    response.ok(res, result);
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { department_id } = req.params;
        const departments = await knex('departments').where({ department_id }).first();
        response.ok(res, departments);
    }
    catch (error) {
        logger('department/show', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            department_id,
            department_name,
            description,
            active,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('departments')
            .insert([{
                department_id,
                department_name,
                description,
                active,
                created_by,
                created_at
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        logger('department/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            department_id,
            department_name,
            description,
            active,
            updated_by,
            updated_at = knex.fn.now()
        } = req.body;

        await knex('departments')
            .update({
                department_id,
                department_name,
                description,
                active,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        logger('department/update', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { department_id } = req.params;
        const res_delete = await knex('departments').where({ department_id }).del();
        response.ok(res, res_delete);
    }
    catch (error) {
        logger('department/destroy', error);
        res.status(500).end();
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    destroy,
}