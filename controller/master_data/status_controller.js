const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { data } = req.query;
        
        let result;
        if (data == 'active') {
            result = await knex('status').where({ active: '1' }).orderBy('id', 'asc');
        }
        else {
            result = await knex('status').orderBy('id', 'asc');
        }
        response.ok(res, result);
    }
    catch (error) {
        logger('status/show', error);
        res.status(500).end();
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const status = await knex('status').where({ id }).first();
        response.ok(res, status);
    }
    catch (error) {
        logger('status/show', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            status,
            description,
            active,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('status')
            .insert([{
                status,
                description,
                active,
                created_by,
                created_at
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        logger('status/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            status,
            description,
            active,
            updated_by,
            updated_at = knex.fn.now()
        } = req.body;

        await knex('status')
            .update({
                status,
                description,
                active,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        logger('status/update', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const res_delete = await knex('status').where({ id }).del();
        response.ok(res, res_delete);
    }
    catch (error) {
        logger('status/destroy', error);
        res.status(500).end();
    }
}


module.exports = {
    index,
    show,
    store,
    update,
    destroy,
}