const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    if (req.method !== 'GET') return res.status(405).end();
    const { data } = req.query;

    let result;
    if (data == 'ticketing') {
        result = await knex('user_level').where({ level_scope: 'ticketing' }).orderBy('id', 'asc');
    }
    else {
        result = await knex('user_level').orderBy('id', 'asc');
    }
    response.ok(res, result);
}

module.exports = { index }