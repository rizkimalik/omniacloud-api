'use strict';
const knex = require('../config/db_connect');
const { response } = require('../helper');

const notification_ticket = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { department_id, user_create, user_level, status } = req.body;
       
        let param_ticket;
        if (user_level === 'L1') {
            param_ticket = `WHERE status='${status}' AND department_id='${department_id}' AND user_create='${user_create}'`;
        }
        else if (user_level === 'L2') {
            param_ticket = `WHERE status='${status}' AND dispatch_department='${department_id}' AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=tickets.ticket_number AND dispatch_to_layer='L2')`;
        }
        else if (user_level === 'L3') {
            param_ticket = `WHERE status='${status}' AND dispatch_department='${department_id}' AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=tickets.ticket_number AND dispatch_to_layer='L3')`;
        }
        else {
            //? user_level = admin
            param_ticket = `WHERE status='${status}'`;
        }

        const tickets = await knex.raw(`SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM tickets ${param_ticket}`);
        response.ok(res, tickets[0]);
    }
    catch (error) {
        response.error(res, error.message, 'notification/notification_ticket');
    }
}

module.exports = {
    notification_ticket,
}