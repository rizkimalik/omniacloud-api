'use strict';
const knex = require('../../config/db_connect');
const axios = require('axios');
const FormData = require('form-data');
const date = require('date-and-time');
const { response, logger, random_string } = require('../../helper');
const { insert_channel_customer } = require('../customer_channel_controller');
const { sentiment_analysis } = require('../sentiment_analysis');
const { update_sent_message_status } = require('./sosial_media');

const twitter_get_token = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const check = await knex('sosmed_channels').count('page_id as jml').where({ page_id: data.user_id, channel: 'Twitter' }).first();

        if (check.jml > 0) {
            data.result = 'Update Success.'
            await knex('sosmed_channels')
                .where({ page_id: data.user_id, channel: 'Twitter' })
                .update({
                    page_name: data.screen_name,
                    page_category: '',
                    channel: 'Twitter',
                    account_id: '',
                    token: data.oauth_token,
                    token_secret: data.oauth_token_secret,
                    user_secret: '',
                    url_api: data[i]?.url_api,
                    updated_at: knex.fn.now(),
                })
        }
        else {
            data.result = 'Insert Success.'
            await knex('sosmed_channels')
                .insert({
                    page_id: data.user_id,
                    page_name: data.screen_name,
                    page_category: '',
                    channel: 'Twitter',
                    account_id: '',
                    token: data.oauth_token,
                    token_secret: data.oauth_token_secret,
                    user_secret: '',
                    url_api: data[i]?.url_api,
                    created_at: knex.fn.now(),
                })
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/twitter_token', error);
    }
}

const twitter_token_detail = async function ({ page_id }) {
    try {
        const result = await knex('sosmed_channels').where({ page_id, channel: 'Twitter' }).first();
        logger('INFO/twitter_token', result);
        return result;
    }
    catch (error) {
        logger('ERROR/twitter_token', error.message);
    }
}

const twitter_get_directmessage = async function (req, res, io) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const value = req.body;
        if (value.for_user_id === value.direct_message_events[0].message_create.sender_id) return res.status(204).end(); //? except self message
        const result = await twitter_directmessage_detail(value);
        if (result) {
            const data = result;
            const now = new Date();
            const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
            const chat = await knex('chats').select('chat_id', 'agent_handle')
                .where({
                    user_id: data.from_detail.screen_name,
                    flag_to: 'customer',
                    flag_end: 'N',
                    channel: 'Twitter_DM',
                    page_id: value.for_user_id
                }).first();
            const analysis = await sentiment_analysis(data.message_data.text);

            data.channel = 'Twitter_DM';
            data.flag_to = 'customer';
            data.page_id = value.for_user_id;
            data.customer_id = await insert_data_customer(data);
            data.chat_id = chat ? chat.chat_id : generate_chatid;
            data.message_type = data.message_data.attachment ? data.message_data.attachment.type : 'text';

            const customer_spam = await knex.raw(`select COUNT(customer_id) as total from customer_account_spam where channel='Twitter_DM' and (customer_id='${data.customer_id}' OR user_id='${data.from_detail.screen_name}')`);
            if (customer_spam[0][0].total > 0) {
                data.agent_handle = 'SPAM';
                data.flag_end = 'Y';
                data.status_chat = 'closed';
            }
            else {
                data.agent_handle = chat ? chat.agent_handle : '';
                data.flag_end = 'N';
                data.status_chat = 'open';
            }

            const values = {
                chat_id: data.chat_id,
                user_id: data.from_detail.screen_name,
                message: data.message_data.text,
                message_type: data.message_type,
                name: data.from_detail.name,
                email: data.from_id,
                message_id: data.id,
                channel: data.channel,
                customer_id: data.customer_id,
                flag_to: data.flag_to,
                status_chat: data.status_chat,
                flag_end: data.flag_end,
                agent_handle: data.agent_handle,
                page_id: data.page_id,
                date_create: new Date(),
                sentiment: analysis?.sentiment,
                objective_score: analysis?.obj_score,
                positive_score: analysis?.pos_score,
                negative_score: analysis?.neg_score,
                // date_create: knex.fn.now(),
                // date_create: data.message_timestamp
            }

            await knex('chats').insert([values]);
            if (data.agent_handle) {
                io.to(data.agent_handle).emit('return-directmessage-twitter', values);
            }
            if (data.message_type !== 'text') {
                // const path_url = await file_manager.DownloadFileURL(data);
                // data.url = path_url;
                // await insert_data_attachment(data);
            }
        }
        else {
            logger('ERROR/twitter_get_directmessage', result);
        }

        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/twitter_get_directmessage', error);
    }
}

const twitter_directmessage_detail = async function (value) {
    try {
        const account = await twitter_token_detail({ page_id: value.for_user_id });
        let data = new FormData();
        data.append('oauth_token', account.token);
        data.append('oauth_token_secret', account.token_secret);
        data.append('id', value.direct_message_events[0].id);

        let response = '';
        await axios({
            method: 'post',
            url: `${account.url_api}/sosial/twitter/twitter/getdirectmessageshow`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(function (result) {
                response = result.data.data;
            })
            .catch(function (error) {
                logger('ERROR/twitter_directmessage_detail', error.message);
            });

        return response;
    }
    catch (error) {
        logger('ERROR/twitter_directmessage_detail', error.message);
    }
}

const twitter_post_directmessage = async function (value) {
    try {
        value.message_id = random_string(20);
        const account = await twitter_token_detail({ page_id: value.page_id });
        let data = new FormData();
        data.append('oauth_token', account.token);
        data.append('oauth_token_secret', account.token_secret);
        data.append('from_id', value.email);
        data.append('message', value.message);
        data.append('type', value.message_type);

        // let response = '';
        await insert_message_agent(value);
        await axios({
            method: 'post',
            url: `${account.url_api}/sosial/twitter/twitter/postdirectmessage`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(async (result) => {
                logger('INFO/twitter_post_directmessage', result.data);
                await update_sent_message_status(value);
            })
            .catch((error) => {
                logger('ERROR/twitter_post_directmessage', error.message);
            });
    }
    catch (error) {
        logger('ERROR/twitter_post_directmessage', error.message);
    }
}

const twitter_get_mention = async function (req, res, io) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const tweet_mention = req.body;
        const data = tweet_mention.tweet_create_events[0];
        if (tweet_mention.for_user_id === data.user.id_str) return res.status(204).end(); //? except self message
        const generate_chatid = date.format(new Date(), 'YYYYMMDDHHmmSSSmmSSS');
        const mention = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: data.user.screen_name,
                flag_to: 'customer',
                flag_end: 'N',
                channel: 'Twitter_Mention',
                page_id: tweet_mention.for_user_id
            }).first();
        const analysis = await sentiment_analysis(data.text);

        data.channel = 'Twitter_Mention';
        data.flag_to = 'customer';
        data.customer_id = await insert_mention_customer(data);
        data.chat_id = mention ? mention.chat_id : generate_chatid;
        data.message_type = data.entities.media ? 'image' : 'text';

        const customer_spam = await knex.raw(`select COUNT(customer_id) as total from customer_account_spam where channel='Twitter_Mention' and (customer_id='${data.customer_id}' OR user_id='${data.user.screen_name}')`);
        if (customer_spam[0][0].total > 0) {
            data.agent_handle = 'SPAM';
            data.flag_end = 'Y';
            data.status_chat = 'closed';
        }
        else {
            data.agent_handle = mention ? mention.agent_handle : '';
            data.flag_end = 'N';
            data.status_chat = 'open';
        }

        const values = {
            chat_id: data.chat_id,
            user_id: data.user.screen_name,
            message: data.text,
            message_type: data.message_type,
            name: data.user.name,
            email: data.user.id_str,
            message_id: data.id_str,
            channel: data.channel,
            customer_id: data.customer_id,
            flag_to: data.flag_to,
            status_chat: data.status_chat,
            flag_end: data.flag_end,
            agent_handle: data.agent_handle,
            page_id: tweet_mention.for_user_id,
            date_create: new Date(),
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
            // date_create: knex.fn.now(),
        }

        await knex('chats').insert([values]);
        if (data.agent_handle) {
            io.to(data.agent_handle).emit('return-directmessage-twitter', values);
        }
        if (data.message_type !== 'text') {
            // const path_url = await file_manager.DownloadFileWhatsapp(data);
            // data.url = path_url;
            // await insert_data_attachment(data);
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/twitter_get_mention', error);
    }
}

const twitter_post_mention = async function (value) {
    try {
        const account = await twitter_token_detail({ page_id: value.page_id });
        let data = new FormData();
        data.append('oauth_token', account.token);
        data.append('oauth_token_secret', account.token_secret);
        data.append('mention_id', value.message_id);
        data.append('message', value.message);
        data.append('media_ids', '');

        await axios({
            method: 'post',
            url: `${account.url_api}/sosial/twitter/twitter/replaytweet`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(async function ({ data }) {
                // console.log(data);
                //? set data callback
                value.message_id = data.mention_id_str;
                value.message = data.mention;
                await insert_message_agent(value);
                // {
                //     "code": 200,
                //     "message": "success",
                //     "data": {
                //         "mention_id": 1643265760670920704,
                //         "mention_id_str": "1643269682399678466",
                //         "mention": "@MalikDev2020 pagi juga, reply by postman2",
                //         "source": "<a href=\"https://omnimoz.com/\" rel=\"nofollow\">keuraisdesk</a>",
                //         "in_reply_to_status_id": 1640193765959958528,
                //         "in_replay_from_tweet": "@Keuraisdesk selamat pagi",
                //         "created_at": "2023-04-04 09:54:22"
                //     }
                // }
            })
            .catch(function (error) {
                logger('ERROR/twitter_post_mention', error);
            });
    }
    catch (error) {
        logger('ERROR/twitter_post_mention', error);
    }
}

const insert_data_customer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ account_id: data.from_detail.screen_name, source: 'Twitter' }).first();
    const channel = await knex('customer_channels').select('customer_id').where({ value_channel: data.from_detail.screen_name, flag_channel: 'Twitter' }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.from_detail.name,
                email: data.from_detail.screen_name + '@twitter.com',
                account_id: data.from_detail.screen_name,
                source: 'Twitter',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);
    }
    if (!channel) {
        await insert_channel_customer({
            customer_id: customer_id,
            value_channel: data.from_detail.screen_name,
            flag_channel: 'Twitter'
        });
    }

    return customer_id;
}

const insert_mention_customer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ account_id: data.user.screen_name, source: 'Twitter' }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.user.name,
                account_id: data.user.screen_name,
                source: 'Twitter',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);

        const channel = await knex('customer_channels').select('customer_id').where({ value_channel: data.user.screen_name, flag_channel: 'Twitter' }).first();
        if (!channel) {
            await insert_channel_customer({
                customer_id: customer_id,
                value_channel: data.user.screen_name,
                flag_channel: 'Twitter'
            });
        }

    }
    return customer_id;
}

const insert_message_agent = async function (data) {
    const analysis = await sentiment_analysis(data.message);

    await knex('chats')
        .insert([{
            chat_id: data.chat_id,
            user_id: data.user_id,
            customer_id: data.customer_id,
            message: data.message,
            message_type: data.message_type,
            message_id: data.message_id,
            name: data.name,
            email: data.email,
            agent_handle: data.agent_handle,
            page_id: data.page_id,
            channel: data.channel,
            flag_to: 'agent',
            status_chat: 'open',
            flag_end: 'N',
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
            date_create: knex.fn.now()
        }]);
}

module.exports = {
    twitter_get_token,
    twitter_get_directmessage,
    twitter_post_directmessage,
    twitter_get_mention,
    twitter_post_mention,
}



