// const path = require('path');
const axios = require('axios');
const date = require('date-and-time');
const knex = require('../../config/db_connect');
const { response, logger, file_manager, random_string } = require('../../helper');
const { insert_channel_customer } = require('../customer_channel_controller');
const { sentiment_analysis } = require('../sentiment_analysis');
const { update_sent_message_status } = require('./sosial_media');

const whatsapp_business_webhook = async function (req, res, io) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { entry } = req.body;
        const data = entry[0].changes[0];
        const now = new Date();
        const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
        data.channel = 'Whatsapp_Business';
        data.flag_to = 'customer';
        data.page_id = data.value.metadata.phone_number_id;
        data.customer_id = await insert_data_customer(data);
        const analysis = await sentiment_analysis(data.value.messages[0].text.body);
        const chat = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: data.value.contacts[0].wa_id,
                flag_to: 'customer',
                flag_end: 'N',
                channel: data.channel,
                page_id: data.page_id
            }).first();
        data.chat_id = chat ? chat.chat_id : generate_chatid;

        const customer_spam = await knex.raw(`select COUNT(customer_id) as total from customer_account_spam where channel='Whatsapp' and (customer_id='${data.customer_id}' OR user_id='${data.value.contacts[0].wa_id}')`);
        if (customer_spam[0][0].total > 0) {
            data.agent_handle = 'SPAM';
            data.flag_end = 'Y';
            data.status_chat = 'closed';
        }
        else {
            data.agent_handle = chat ? chat.agent_handle : '';
            data.flag_end = 'N';
            data.status_chat = 'open';
        }

        const values = {
            chat_id: data.chat_id,
            user_id: data.value.contacts[0].wa_id,
            message: data.value.messages[0].text.body,
            message_type: data.value.messages[0].type,
            name: data.value.contacts[0].profile.name,
            email: data.value.contacts[0].wa_id,
            page_id: data.page_id,
            message_id: data.value.messages[0].id,
            channel: data.channel,
            customer_id: data.customer_id,
            flag_to: data.flag_to,
            status_chat: data.status_chat,
            flag_end: data.flag_end,
            agent_handle: data.agent_handle,
            date_create: new Date(),
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
        }

        await knex('chats').insert([values]);

        if (data.message_type !== 'text') {
            // save attachment
        }
        if (data.agent_handle) {
            io.to(data.agent_handle).emit('return-message-whatsapp', values);
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/whatsapp_business_webhook', error.message);
        res.status(500).end();
    }
}

// function non http
const whatsapp_business_send_messages = async function (data) {
    try {
        const account = await whatsapp_token_detail({ page_id: data.page_id });
        data.message_id = random_string(20);
        let values = '';
        let upload_status = '';

        if (data.message_type === 'text') {
            upload_status = 'true';
            values = {
                messaging_product: "whatsapp",
                to: data.user_id,
                type: "text",
                text: {
                    body: data.message
                }
            }
        }
        else if (data.message_type === 'image') {
            upload_status = await upload_attachment_file(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                image: {
                    link: data.attachment[0].file_base + '/' + data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }
        else if (data.message_type === 'document') {
            upload_status = await upload_attachment_file(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                document: {
                    link: data.attachment[0].file_base + '/' + data.attachment[0].file_url,
                    caption: data.message
                }
            }

        }

        if (upload_status) {
            await insert_data_message(data);
            await axios({
                url: `${account.url_api}/${data.page_id}/messages`,
                method: 'POST',
                maxBodyLength: Infinity,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + account.token,
                },
                data: JSON.stringify(values),
            })
                .then(async (result) => {
                    // data.reply_id = result.data.messages[0].id;
                    logger('INFO/whatsapp_business/send_messages', result.data);
                    await update_sent_message_status(data);
                })
                .catch((error) => {
                    logger('ERROR/whatsapp_business_send_messages', error.message);
                });
        }
    }
    catch (error) {
        logger('ERROR/whatsapp_business_send_messages', error.message);
    }
}

const insert_data_customer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ phone_number: data.value.contacts[0].wa_id }).first();
    const channel = await knex('customer_channels').select('customer_id').where({ value_channel: data.value.contacts[0].wa_id }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.value.contacts[0].profile.name,
                phone_number: data.value.contacts[0].wa_id,
                source: 'Whatsapp',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);
    }
    if (!channel) {
        await insert_channel_customer({ customer_id, value_channel: data.value.contacts[0].wa_id, flag_channel: 'Phone' });
    }
    return customer_id;
}

const insert_data_message = async function (data) {
    const analysis = await sentiment_analysis(data.message);

    await knex('chats')
        .insert([{
            chat_id: data.chat_id,
            user_id: data.user_id,
            customer_id: data.customer_id,
            message: data.message,
            message_type: data.message_type,
            message_id: data.message_id,
            name: data.name,
            email: data.email,
            agent_handle: data.agent_handle,
            channel: 'Whatsapp_Business',
            flag_to: 'agent',
            status_chat: 'open',
            flag_end: 'N',
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
            date_create: knex.fn.now()
        }]);
}

const whatsapp_token_detail = async function ({ page_id }) {
    try {
        const result = await knex('sosmed_channels').where({ page_id, channel: 'Whatsapp' }).first();
        logger('INFO/whatsapp_business_token_detail', result);
        return result;
    }
    catch (error) {
        logger('ERROR/whatsapp_business_token_detail', error.message);
    }
}

module.exports = {
    whatsapp_business_webhook,
    whatsapp_business_send_messages,
}