const querystring = require('querystring');
const axios = require('axios');
const { logger } = require('../helper');

exports.sentiment_analysis = async (text_value) => {
    try {
        const msg_no_special_chars = text_value.replace(/[^a-zA-Z0-9 ]/g, '');
        const url = process.env.URL_SENTIMENT;
        const data = querystring.stringify({
            teks_id: msg_no_special_chars,
        });
        const headers = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-Api-Key': 'yourSecretKeyIsAistech',
            }
        }
        const response = await axios.post(url, data, headers);

        // return response.data; //? result data
        // return { sentiment: 'neutral', neg_score: 0, obj_score: 0, pos_score: 0 };
        return {
            sentiment: response.data.sentiment,
            obj_score: (response.data.obj_score).toFixed(3),
            pos_score: (response.data.pos_score).toFixed(3),
            neg_score: (response.data.neg_score).toFixed(3),
        } //? result data
    }
    catch (error) {
        logger('sentiment_analitycs', error);
    }
}