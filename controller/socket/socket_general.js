module.exports = function (socket) {
    socket.on('transfer-assign-agent', (data) => {
        socket.to(data.agent_handle).emit('return-transfer-assign-agent', data);
    });

    socket.on('send-message-error', (data) => {
        socket.to(data.agent_handle).emit('return-message-error', data);
    });
}