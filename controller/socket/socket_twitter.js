'use strict';
const { twitter_post_directmessage, twitter_post_mention } = require("../omnichannel/twitter");

module.exports = function (socket) {
    socket.on('send-directmessage-twitter', (data) => {
        twitter_post_directmessage(data);
    });
    
    socket.on('return-directmessage-twitter', (data) => {
        socket.to(data.agent_handle).emit('return-directmessage-twitter', data); //notused
    });
    
    socket.on('send-mention-twitter', (data) => {
        twitter_post_mention(data);
    });
    
    socket.on('return-mention-twitter', (data) => {
        socket.to(data.agent_handle).emit('return-mention-twitter', data); //notused
    });

}