"use strict";
const path = require('path');
const fs = require('fs');
const https = require('https');
const logger = require('./logger');

const DownloadFileURL = async function ({ channel, file_name, file_url }) {
    try {
        const directory = `./${process.env.ATTACHMENT_DIR}/${channel}/`;
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        const destination = `${directory + file_name}`;

        https.get(file_url, (res) => {
            const filePath = fs.createWriteStream(destination);
            res.pipe(filePath);
            filePath.on('finish', () => {
                filePath.close();
                console.log(`Download attachment ${channel} completed.`);
                return destination.replace('./', '');
            })
        }).on('error', error => {
            fs.unlink(destination);
            logger('file_manager/DownloadFileURL', `Gagal mendownload file: ${error.message}`);

        });
        return destination.replace('./', '');

    } catch (error) {
        console.log(error.message)
        logger('file_manager/DownloadFileURL', error.message);
    }
}

const UploadAttachment = async function (value) {
    logger('file_manager/UploadAttachment', value);
    const { channel, attachment, file_name, file_size } = value;

    try {
        const extension = path.extname(file_name);
        const allowedExtensions = /png|jpeg|jpg|gif|svg|pdf|xls|xlsx|doc/;
        const directory = `./${process.env.ATTACHMENT_DIR}/${channel}/`;
        const url = `${directory + file_name}`;
        const max_file = 10000000; // 10MB

        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        if (file_size > max_file) {
            logger('file_manager/UploadAttachment', `Max File Size ${max_file / 1000}MB`);
        }
        if (!allowedExtensions.test(extension)) {
            logger('file_manager/UploadAttachment', 'Invalid Allow File type: ' + allowedExtensions);
        }

        fs.writeFile(url, attachment, function (err) {
            if (err) {
                logger('file_manager/UploadAttachment', err);
                return console.log(err);
            }
        });
        logger('file_manager/UploadAttachment', url);

        return url;
    } catch (error) {
        console.log(error)
        logger('file_manager/UploadAttachment', error);
    }
}

const DownloadFromBase64 = async function (value) {
    const { channel, message_type, message_id, message_raw } = value;
    try {
        const directory = `./${process.env.ATTACHMENT_DIR}/${channel}/`;
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }

        let dataBase64 = '';
        let fileName = '';
        if (message_type === 'image') {
            fileName = message_id + '.jpeg';
            dataBase64 = message_raw.imageMessage.jpegThumbnail;
        }
        const url = `${directory + fileName}`;

        fs.writeFile(url, dataBase64, 'base64', function (err) {
            if (err) {
                logger('file_manager/DownloadAttachment', err);
                return console.log(err);
            }
        });

        return url;
    } catch (error) {
        console.log(error)
        logger('file_manager/DownloadAttachment', error);
    }
}

function read_dir_file(dir, files_) {
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files) {
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()) {
            read_dir_file(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}

// DownloadFileURL('Call','halo.wav','https://pabx.keurais.com:8887/api/files/676473928905916416/data')

module.exports = {
    DownloadFileURL,
    UploadAttachment,
    DownloadFromBase64,
    read_dir_file,
}