
const up = function(knex) {
    return knex.schema.createTable('departments', function(table){
        table.increments('id').primary();
        table.string('department_id', 20).unique().notNullable();
        table.string('department_name', 50).notNullable();
        table.string('description');
        table.boolean('active');
        table.string('created_by', 50);
        table.string('updated_by', 50);
        table.timestamps();
    })
};

const down = function(knex) {
    return knex.schema.dropTable('departments');
};

module.exports = {up, down}