
const up = function(knex) {
    return knex.schema.createTable('threads', function(table){
        table.increments('id').primary();
        table.string('thread_id', 100);
        table.string('thread_channel', 100).notNullable();
        table.string('account', 150).nullable();
        table.string('agent_id', 100).nullable();
        table.text('subject').nullable();
        table.string('ticket_number', 100).nullable();
        table.string('customer_id', 100).nullable();
        table.string('created_by', 100).nullable();
        table.string('updated_by', 100).nullable();
        table.timestamps();
    })
};

const down = function(knex) {
    return knex.schema.dropTable('threads');
};

module.exports = {up, down}